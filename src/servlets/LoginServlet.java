package servlets;

import dao.UserDAO;
import domain.User;
import parsers.UserRequestParser;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "LoginServlet")
public class LoginServlet extends HttpServlet {
    protected  void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User formUser = UserRequestParser.Parse(request),
                dbUser = null;

        try {
            dbUser = this.getUserFromDbByLogin(formUser.getLogin());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        if(dbUser == null ||
                !this.passwordIsCorrect(formUser.getPassword(), dbUser.getPassword())) {
            response.sendRedirect("invalidLoginOrPassword.jsp");
            return;
        }

        HttpSession session = request.getSession(true);
        session.setAttribute("Privilege", dbUser.getPrivilege());
        session.setAttribute("UserName", dbUser.getLogin());

        response.sendRedirect("userProfile.jsp");
        return;
    }

    private User getUserFromDbByLogin(String login) throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
        User user = null;

        UserDAO dao = new UserDAO();
        user = dao.getUserByLogin(login);

        return user;
    }
    private boolean passwordIsCorrect(String formPassword, String dbPassword) {
        return formPassword.equalsIgnoreCase(dbPassword);
    }
}
