package servlets;

import dao.UserDAO;
import domain.User;
import parsers.UserRequestParser;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

public class RegistrationServlet extends javax.servlet.http.HttpServlet {
    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws IOException {
    }
    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        User user = UserRequestParser.Parse(request);

        insert(user);
        response.sendRedirect("index.jsp");
    }


    private void insert(User user) {
        UserDAO userDAO = null;
        try {
            userDAO = new UserDAO();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }


        try {
            userDAO.insert(user);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
