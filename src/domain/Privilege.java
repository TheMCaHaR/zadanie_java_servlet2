package domain;

public enum Privilege {
    Anonim(1), User(2), Premium(3), Admin(4);

    private int id;

    private Privilege(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }
}
