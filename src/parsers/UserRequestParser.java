package parsers;

import domain.Privilege;
import domain.User;

import javax.servlet.http.HttpServletRequest;

public class UserRequestParser {
    private UserRequestParser() { }

    public static User Parse(HttpServletRequest request) {
        User user = new User();

        user.setLogin(request.getParameter("login"));
        user.setPassword(request.getParameter("password"));
        user.setEmail(request.getParameter("email"));
        user.setPrivilege(Privilege.User);

        return user;
    }
}
