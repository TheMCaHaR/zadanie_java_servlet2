package parsers;

import domain.Privilege;
import domain.User;

public class SqlUserParser {
    private SqlUserParser() { }
    public static String insert(User user) {
        String query = "insert into t_user (Login, Password, Email, Privileges_Id) values ('" +
                user.getLogin() + "', '" +
                user.getPassword() + "', '" +
                user.getEmail() + "', '" +
                user.getPrivilege().getId() + "');";
        return query;
    }

    public static String countByLogin(String login) {
        String query = "select count(id) from t_user where Login = '" + login + "';";
        return query;
    }
    public static String getByLogin(String login) {
        String query = "select * from t_user where Login = '" + login + "';";
        return query;
    }
    public static String getAll() {
        return "select * from t_user;";
    }
    public static String updatePrivilege(String login, Privilege privilege) {
        return "update t_user set Privileges_Id = '" + privilege.getId() + "' where Login = '" + login + "';";
    }
}
