package dao;

import domain.Privilege;
import domain.User;
import parsers.SqlUserParser;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDAO {
    private final static String DBURL = "jdbc:mysql://localhost/servlet2";
    private final static String DBUSER = "root";
    private final static String DBPASS = "root";
    private final static String DBDRIVER = "com.mysql.jdbc.Driver";

    private Connection connection;
    private Statement statement;
    private String query;

    public UserDAO() throws ClassNotFoundException, SQLException, IllegalAccessException, InstantiationException {
        Class.forName(DBDRIVER).newInstance();
        connection = DriverManager.getConnection(DBURL, DBUSER, DBPASS);
        statement = connection.createStatement();
    }

    public void insert(User user) throws SQLException {
        this.query = SqlUserParser.insert(user);
        this.statement.executeUpdate(this.query);
    }
    public User getUserByLogin(String login) throws SQLException {
        User user = null;

        if(this.userExist(login))
            user = this.getUser(login);
        return user;
    }
    public List<User> getAll() throws SQLException {
        this.query = SqlUserParser.getAll();
        ResultSet rs = this.statement.executeQuery(this.query);

        List<User> users = new ArrayList<>();

        while(rs.next()) {
            User user = new User();
            user.setLogin(rs.getString("Login"));
            user.setPassword(rs.getString("Password"));
            user.setEmail(rs.getString("Email"));
            user.setPrivilege(
                    Privilege.values()[rs.getInt("Privileges_Id") - 1]);
            users.add(user);
        }
        return users;
    }
    public void updatePrivilege(String login, Privilege newPrivilege) throws SQLException {
        this.query = SqlUserParser.updatePrivilege(login, newPrivilege);
        this.statement.executeUpdate(this.query);
    }

    public boolean userExist(String login) throws SQLException {
        this.query = SqlUserParser.countByLogin(login);
        ResultSet rs = this.statement.executeQuery(this.query);

        boolean exist = false;
        while(rs.next())
            if(rs.getInt("count(id)") == 1)
                exist = true;

        return exist;
    }
    private User getUser(String login) throws SQLException {
        this.query = SqlUserParser.getByLogin(login);
        ResultSet rs = this.statement.executeQuery(this.query);

        User user = new User();
        while(rs.next()) {
            user.setLogin(
                    rs.getString("Login"));
            user.setPassword(
                    rs.getString("Password"));
            user.setEmail(
                    rs.getString("Email"));
            user.setPrivilege(
                    Privilege.values()[rs.getInt("Privileges_Id") - 1]);
        }
        return user;
    }
}
