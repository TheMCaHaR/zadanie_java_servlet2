<%@ page import="information.Application" %>
<%@ page import="domain.Privilege" %>
<%@include file="shared/bootstrap.html" %>
<% String applicationTitle = Application.getTitle(),
        userName = "anonim";
    Privilege privilege = Privilege.Anonim;

    if(session.getAttribute("UserName") != null)
        userName = (String)session.getAttribute("UserName");
    if(session.getAttribute("Privilege") != null)
        privilege = Privilege.valueOf(session.getAttribute("Privilege").toString());
%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title><% out.print(applicationTitle); %></title>
</head>
<body>
<!-- navbar -->
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Servlet 2</a>
        </div>
        <div>
            <ul id="menuBar" class="nav navbar-nav">
                <li class="active"><a href="index.jsp">Strona główna</a></li>
                <li ><a href = "users" > Lista użytkowników</a ></li >
                <% if(privilege != Privilege.Anonim) {
                    out.print("<li><a href = \"userProfile.jsp\" > Profil </a ></li>");
                    if(privilege != Privilege.User)
                        out.print("<li ><a href = \"premium.jsp\" > Premium </a ></li >");
                    if(privilege == Privilege.Admin)
                        out.print("<li ><a href = \"privileges.jsp\" >Nadawanie uprawnień</a ></li >");
                } %>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <% if(privilege != Privilege.Anonim) {
                    out.print("<li><a href=\"userProfile.jsp\"><span class=\"glyphicon glyphicon-wrench\"></span>"
                            + userName +
                            "</a></li>");
                } %>
                <li><a href="registrationForm.jsp"><span class="glyphicon glyphicon-user"></span> Rejestracja</a></li>
                <li><a href="login.jsp"><span class="glyphicon glyphicon-log-in"></span>
                    <% if(privilege != Privilege.Anonim)
                        out.print("Wyloguj");
                    else
                        out.print("Zaloguj");
                    %>
                </a></li>
            </ul>
        </div>
    </div>
</nav>
<!-- -->

<div class="container">
    <form class="form-horizontal" role="form" method="post" action="privileges">
        <div class="form-group">
            <label class="control-label col-sm-2" for="userLogin">Login:</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="userLogin" name="userLogin" placeholder="Wprowadź login">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="privilege">Hasło:</label>
            <div class="col-sm-10">
                <select name="privilege" id="privilege" required>
                    <option value="Premium">Premium</option>
                    <option value="User">User</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Wyślij</button>
            </div>
        </div>
    </form>
</body>
</html>